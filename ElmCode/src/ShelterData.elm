module ShelterData exposing (Animal, Coat, Intake, Outcome, Sex(..), Stay, TransferType(..), coatDecoder, decodeIntake, decodeOutcome, stayErrors)

import Calendar exposing (RawDate)
import Clock exposing (RawTime)
import Csv
import Csv.Decode
import DateTime exposing (fromRawParts)
import Json.Decode
import List.Extra
import Time exposing (Posix)



--############################################################################
--Typen
--describes an animal, only the attributes that shouldn't change


type alias Animal =
    { animalId : String
    , breeds : List String
    , purebreed : Bool
    , sex : Maybe Sex
    , animalType : String
    , coat : Coat
    }


type alias AnimalTemp =
    { animalId : String
    , breeds : List String
    , purebreed : Bool
    , sex : Maybe Sex
    , animalType : String
    , coat : Coat
    }



--the sex of the animal: male or female for most species found in a shelter


type Sex
    = Female
    | Male



--describes the appearance of the animal: colors and patterns in decreasing order by fraction


type alias Coat =
    { patterns : List String
    , colors : List String
    }



-- describes the circumstances of the animals intake and the attributes of the animal that can change (fixed/intact; age)


type alias Intake =
    { animal : Animal
    , inDate : Posix
    , age : Maybe Int
    }


type alias IntakeTemp =
    { animal : AnimalTemp
    , inDate : Posix
    , inMonth : Posix
    , age : Maybe Int
    }



-- describes the circumstances of the animals Outcome and the attributes of the animal that can change (fixed/intact; age)


type alias Outcome =
    { animal : Animal
    , outDate : Posix
    , outcomeType : String
    }


type alias OutcomeTemp =
    { animal : Animal
    , outDate : Posix
    , outMonth : Posix
    , outcomeType : String
    }



-- decode json that contains coatPatterns and coatColors as a list into a Coat type


coatDecoder : Json.Decode.Decoder Coat
coatDecoder =
    Json.Decode.map2
        (\colors patterns ->
            { colors = List.map String.toLower colors
            , patterns = List.map String.toLower patterns
            }
        )
        (Json.Decode.field "coatColors" <| Json.Decode.list Json.Decode.string)
        (Json.Decode.field "coatPatterns" <| Json.Decode.list Json.Decode.string)



--split String into Lists of predefined Coat patterns and Coat colors : Coat type
--unknown words in second list


stringToMCoat : List String -> List String -> String -> ( Maybe Coat, List String )
stringToMCoat patternlist colorlist raw =
    let
        words =
            List.foldl (\s l -> l ++ String.split "/" s) [] (String.words <| String.toLower raw)

        colors =
            List.filter (\a -> List.any (\e -> e == a) colorlist) words

        patterns =
            List.filter (\a -> List.any (\e -> e == a) patternlist) words

        unknown =
            List.filter (\a -> List.all (\e -> e /= a) patternlist && List.all (\e -> e /= a) colorlist) words
    in
    case patterns of
        [] ->
            case colors of
                [] ->
                    ( Nothing
                    , unknown
                    )

                [ _ ] ->
                    ( Just
                        { patterns = [ "solid" ]
                        , colors = colors
                        }
                    , unknown
                    )

                [ _, _ ] ->
                    ( Just
                        { patterns = [ "bicolor" ]
                        , colors = colors
                        }
                    , unknown
                    )

                _ ->
                    ( Just
                        { patterns = [ "multicolor" ]
                        , colors = colors
                        }
                    , unknown
                    )

        _ ->
            ( Just
                { patterns = patterns
                , colors = colors
                }
            , unknown
            )



--convert String to Animal Sex and Bool whether it is intact or not (or unknown)


stringToSexIntact : String -> ( Maybe Sex, Maybe Bool )
stringToSexIntact raw =
    let
        words =
            List.foldl (\s l -> l ++ String.split "/" s) [] (String.words <| String.toLower raw)

        mSex : Maybe Sex
        mSex =
            if List.any (\w -> w == "female") words then
                Just Female

            else if List.any (\w -> w == "male") words then
                Just Male

            else
                Nothing

        mIntact : Maybe Bool
        mIntact =
            if List.any (\w -> w == "intact") words then
                Just True

            else if List.any (\w -> w == "spayed" || w == "neutered") words then
                Just False

            else
                Nothing
    in
    ( mSex, mIntact )



--convert a String to a Time.Posix if possible, else nothing
--String Format: "%m/%d/%Y %I:%M:%S %p"
--example: "08/15/2016 05:52:00 PM" -> Posix 1471283520000
--use utc, convert am pm to military time


stringDateTimeToPosix : String -> Maybe Posix
stringDateTimeToPosix raw =
    let
        words =
            String.words raw

        --split an whitespace
        --sollte 3 Teile ergeben; ersten an / und zweiten an : splitten jeweils wiederum 3
        --sonst Fehler -> gib Nothing zurueck in beiden cases
        --wenn moeglich Datum erst in Rohdaten konvertieren, diese wenn Datum existiert zu Posix
    in
    case words of
        [ dateString, timeString, amPm ] ->
            case ( String.split "/" dateString, String.split ":" timeString, String.toLower amPm ) of
                ( [ monthStr, dayStr, yearStr ], [ hourStr, minStr, secStr ], ampm ) ->
                    let
                        maybeDateraw : Maybe RawDate
                        maybeDateraw =
                            Maybe.map3 (\d m y -> { day = d, month = m, year = y })
                                (String.toInt dayStr)
                                (Maybe.andThen intToMonth (String.toInt monthStr))
                                (String.toInt yearStr)

                        maybeTimeraw : Maybe RawTime
                        maybeTimeraw =
                            Maybe.map3
                                (\htemp m s ->
                                    let
                                        --Sonderbehandlung fuer Stunden mit AM, PM (vorher lowercase)
                                        h =
                                            case ( htemp, ampm ) of
                                                ( 12, "am" ) ->
                                                    0

                                                ( 12, "pm" ) ->
                                                    12

                                                ( a, "pm" ) ->
                                                    a + 12

                                                _ ->
                                                    htemp
                                    in
                                    { hours = h
                                    , minutes = m
                                    , seconds = s
                                    , milliseconds = 0
                                    }
                                )
                                (String.toInt hourStr)
                                (String.toInt minStr)
                                (String.toInt secStr)
                    in
                    Maybe.map DateTime.toPosix <|
                        Maybe.andThen (\( date, time ) -> fromRawParts date time) <|
                            Maybe.map2 (\a b -> ( a, b )) maybeDateraw maybeTimeraw

                _ ->
                    Nothing

        _ ->
            Nothing


stringDateToPosix : String -> Maybe Posix
stringDateToPosix raw =
    let
        words =
            String.words raw

        --split an whitespace
        --sollte 1 Teil ergeben; an / splitten wiederum 3
        --sonst Fehler -> gib Nothing zurueck in beiden cases
        --wenn moeglich Datum erst in Rohdaten konvertieren, diese wenn Datum existiert zu Posix
    in
    case words of
        [ dateString ] ->
            case String.split "/" dateString of
                [ monthStr, dayStr, yearStr ] ->
                    let
                        maybeDateraw : Maybe RawDate
                        maybeDateraw =
                            Maybe.map3 (\d m y -> { day = d, month = m, year = y })
                                (String.toInt dayStr)
                                (Maybe.andThen intToMonth (String.toInt monthStr))
                                (String.toInt yearStr)

                        timeraw : RawTime
                        timeraw =
                            { hours = 0
                            , minutes = 0
                            , seconds = 0
                            , milliseconds = 0
                            }
                    in
                    Maybe.map DateTime.toPosix <|
                        Maybe.andThen (\( date, time ) -> fromRawParts date time) <|
                            Maybe.map (\a -> ( a, timeraw )) maybeDateraw

                _ ->
                    Nothing

        _ ->
            Nothing



--convert Int to a Time.Month if possible, else Nothing


intToMonth : Int -> Maybe Time.Month
intToMonth i =
    case i of
        1 ->
            Just Time.Jan

        2 ->
            Just Time.Feb

        3 ->
            Just Time.Mar

        4 ->
            Just Time.Apr

        5 ->
            Just Time.May

        6 ->
            Just Time.Jun

        7 ->
            Just Time.Jul

        8 ->
            Just Time.Aug

        9 ->
            Just Time.Sep

        10 ->
            Just Time.Oct

        11 ->
            Just Time.Nov

        12 ->
            Just Time.Dec

        _ ->
            Nothing



--convert a String with the animals age ("2 weeks") to its age in days


stringToAgeInDays : String -> Maybe Int
stringToAgeInDays raw =
    let
        words =
            String.words raw
    in
    case words of
        [ countStr, unit ] ->
            let
                mult : Maybe Float
                mult =
                    case String.toLower unit of
                        "years" ->
                            Just 365.25

                        "year" ->
                            Just 365.25

                        "months" ->
                            Just 30.4375

                        -- average day count in a month: (365*3+366)%(12*4)
                        "month" ->
                            Just 30.4375

                        "weeks" ->
                            Just 7

                        "week" ->
                            Just 7

                        "days" ->
                            Just 1

                        "day" ->
                            Just 1

                        _ ->
                            Nothing

                count : Maybe Int
                count =
                    String.toInt countStr
            in
            Maybe.map2 (\a b -> round (a * toFloat b)) mult count

        _ ->
            Nothing



--convert a String with the animals breed into a list of its breeds and a bool if the animal is purebred
--a "Beagle Mix" is not purebred, but the list will only contain "Beagle"
--"Staffordshire/English Bulldog" is not purebred, the list contains two breeds
--cats rarely have breeds at all, most are "domestic ..." and dont count as purebred
--other animals will be proccesses like dogs


stringToBreeds : String -> String -> ( List String, Bool )
stringToBreeds atype raw =
    let
        mix =
            String.contains "mix" <| String.toLower raw

        domestic =
            (String.contains "domestic" <| String.toLower raw) && String.toLower atype == "cat"

        parts =
            List.map
                (\s -> String.trim <| String.replace "Mix" "" <| String.replace "mix" "" s)
            <|
                String.split "/" raw
    in
    ( parts, not (mix || domestic || List.length parts /= 1) )



--Animal ID,Name,DateTime,MonthYear,Found Location,Intake Type,Intake Condition,Animal Type,Sex upon Intake,Age upon Intake,Breed,Color


decodeIntakeTemp : List String -> List String -> Csv.Decode.Decoder (IntakeTemp -> IntakeTemp) IntakeTemp
decodeIntakeTemp patterns colors =
    Csv.Decode.map
        (\id dt my at sui aui b c ->
            let
                ( sex, intact ) =
                    stringToSexIntact sui

                ( breeds, pure ) =
                    stringToBreeds at b

                animal : AnimalTemp
                animal =
                    { animalId = id
                    , breeds = breeds
                    , purebreed = pure
                    , sex = sex
                    , animalType = at
                    , coat = c
                    }
            in
            { animal = animal
            , inDate = dt
            , inMonth = my
            , age = stringToAgeInDays aui
            }
        )
        (Csv.Decode.field "Animal ID" Ok
            |> Csv.Decode.andMap
                (Csv.Decode.field "DateTime" (\s -> Result.fromMaybe ("The DateTime " ++ s ++ " is not valid.") <| stringDateTimeToPosix s))
            |> Csv.Decode.andMap
                (Csv.Decode.field "MonthYear" (\s -> Result.fromMaybe ("The DateTime " ++ s ++ " is not valid.") <| stringDateTimeToPosix s))
            |> Csv.Decode.andMap
                (Csv.Decode.field "Animal Type" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Sex upon Intake" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Age upon Intake" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Breed" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Color"
                    (\s ->
                        let
                            ( mc, l ) =
                                stringToMCoat patterns colors s

                            err =
                                case l of
                                    [] ->
                                        "The Color " ++ s ++ " is not a valid color."

                                    h :: [] ->
                                        "Unknown word in Color field: "
                                            ++ h
                                            ++ " Is it a pattern or a color?"

                                    h :: rest ->
                                        "Unknown words in Color field: "
                                            ++ List.foldl (\x y -> x ++ ", " ++ y) h rest
                                            ++ " Are they patterns or colors?"
                        in
                        Result.fromMaybe
                            err
                        <|
                            mc
                    )
                )
        )


decodeIntake : String -> List String -> List String -> ( Maybe (List Intake), List String )
decodeIntake raw patterns colors =
    let
        indataRes : Result Csv.Decode.Errors (List IntakeTemp)
        indataRes =
            Csv.Decode.decodeCsv
                (decodeIntakeTemp patterns colors)
                (Csv.parse raw)

        ( data, errors ) =
            case indataRes of
                Ok list ->
                    let
                        filteranimal : AnimalTemp -> Maybe Animal
                        filteranimal at =
                            Just at

                        filterIntake : IntakeTemp -> ( Maybe Intake, Maybe String )
                        filterIntake it =
                            if it.inDate == it.inMonth then
                                ( Maybe.map
                                    (\e ->
                                        { animal = e
                                        , inDate = it.inDate
                                        , age = it.age
                                        }
                                    )
                                    (filteranimal it.animal)
                                , Nothing
                                )

                            else
                                ( Nothing, Just <| "DateTime and Monthyear contain different values for AnimalId " ++ it.animal.animalId ++ " in Intakes" )

                        mappedList =
                            List.map filterIntake list

                        filteredList =
                            List.Extra.unique (List.filterMap Tuple.first mappedList)

                        errorList =
                            List.filterMap Tuple.second mappedList
                    in
                    case errorList of
                        [] ->
                            ( Just filteredList, [] )

                        _ ->
                            ( Just filteredList, "Could not parse all Animals: " :: errorList )

                Err (Csv.Decode.CsvErrors e) ->
                    ( Nothing, List.Extra.unique e )

                Err (Csv.Decode.DecodeErrors e) ->
                    ( Nothing, List.Extra.unique <| List.map Tuple.second e )
    in
    ( data, errors )


decodeOutcomeTemp : List String -> List String -> Csv.Decode.Decoder (OutcomeTemp -> OutcomeTemp) OutcomeTemp
decodeOutcomeTemp patterns colors =
    Csv.Decode.map
        (\id dt my ot at suo b c ->
            let
                ( sex, intact ) =
                    stringToSexIntact suo

                ( breeds, pure ) =
                    stringToBreeds at b

                animal : AnimalTemp
                animal =
                    { animalId = id
                    , breeds = breeds
                    , purebreed = pure
                    , sex = sex
                    , animalType = at
                    , coat = c
                    }
            in
            { animal = animal
            , outDate = dt
            , outMonth = my
            , outcomeType = ot
            }
        )
        (Csv.Decode.field "Animal ID" Ok
            |> Csv.Decode.andMap
                (Csv.Decode.field "DateTime" (\s -> Result.fromMaybe ("The DateTime " ++ s ++ " is not valid.") <| stringDateTimeToPosix s))
            |> Csv.Decode.andMap
                (Csv.Decode.field "MonthYear" (\s -> Result.fromMaybe ("The DateTime " ++ s ++ " is not valid.") <| stringDateTimeToPosix s))
            |> Csv.Decode.andMap
                (Csv.Decode.field "Outcome Type" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Animal Type" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Sex upon Outcome" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Breed" Ok)
            |> Csv.Decode.andMap
                (Csv.Decode.field "Color"
                    (\s ->
                        let
                            ( mc, l ) =
                                stringToMCoat patterns colors s

                            err =
                                case l of
                                    [] ->
                                        "The Color " ++ s ++ " is not a valid color."

                                    h :: [] ->
                                        "Unknown word in Color field: "
                                            ++ h
                                            ++ " Is it a pattern or a color?"

                                    h :: rest ->
                                        "Unknown words in Color field: "
                                            ++ List.foldl (\x y -> x ++ ", " ++ y) h rest
                                            ++ " Are they patterns or colors?"
                        in
                        Result.fromMaybe
                            err
                        <|
                            mc
                    )
                )
        )


decodeOutcome : String -> List String -> List String -> ( Maybe (List Outcome), List String )
decodeOutcome raw patterns colors =
    let
        outdataRes : Result Csv.Decode.Errors (List OutcomeTemp)
        outdataRes =
            Csv.Decode.decodeCsv
                (decodeOutcomeTemp patterns colors)
                (Csv.parse raw)

        ( data, errors ) =
            case outdataRes of
                Ok list ->
                    let
                        filteranimal : AnimalTemp -> Maybe Animal
                        filteranimal at =
                            Just at

                        filterOutcome : OutcomeTemp -> ( Maybe Outcome, Maybe String )
                        filterOutcome ot =
                            if ot.outDate == ot.outMonth then
                                ( Maybe.map
                                    (\e ->
                                        { animal = e
                                        , outDate = ot.outDate
                                        , outcomeType = ot.outcomeType
                                        }
                                    )
                                    (filteranimal ot.animal)
                                , Nothing
                                )

                            else
                                ( Nothing, Just <| "DateTime and Monthyear contain different values for AnimalId " ++ ot.animal.animalId ++ " in Outcomes" )

                        mappedList =
                            List.map filterOutcome list

                        filteredList =
                            List.Extra.unique (List.filterMap Tuple.first mappedList)

                        errorList =
                            List.filterMap Tuple.second mappedList
                    in
                    case errorList of
                        [] ->
                            ( Just filteredList, [] )

                        _ ->
                            ( Just filteredList, "Could not parse all Animals: " :: errorList )

                Err (Csv.Decode.CsvErrors e) ->
                    ( Nothing, List.Extra.unique e )

                Err (Csv.Decode.DecodeErrors e) ->
                    ( Nothing, List.Extra.unique <| List.map Tuple.second e )
    in
    ( data, errors )



--##############################################################################


type alias ReducedIntake =
    { age : Maybe Int
    }


type alias ReducedOutcome =
    { outcomeType : String
    }


type TransferType
    = In ReducedIntake
    | Out ReducedOutcome


type alias Transfer =
    { animal : Animal
    , date : Posix
    , kind : TransferType
    }


transferListFromInOut : List Intake -> List Outcome -> List Transfer
transferListFromInOut ins outs =
    List.map
        (\i ->
            { kind =
                In
                    { age = i.age
                    }
            , date = i.inDate
            , animal = i.animal
            }
        )
        ins
        ++ List.map
            (\o ->
                { kind =
                    Out
                        { outcomeType = o.outcomeType
                        }
                , date = o.outDate
                , animal = o.animal
                }
            )
            outs


type alias Stay =
    { animal : Animal
    , durationDays : Int
    , inDay : Posix
    , inAge : Maybe Int
    , outcomeType : String
    }


transfersToStays : List Transfer -> ( List Stay, List String )
transfersToStays allTransfers =
    let
        transferlists : List ( Maybe (List Transfer), Maybe String )
        transferlists =
            List.map
                (\( a, l ) ->
                    if List.all (\e -> e.animal == a.animal) l then
                        ( Just (List.sortBy (\x -> Time.posixToMillis x.date) (a :: l)), Nothing )

                    else
                        ( Nothing, Just ("Consictency constraint violated for animal: " ++ a.animal.animalId ++ "Animal traits not uniform.") )
                )
                (List.Extra.gatherEqualsBy (\x -> x.animal.animalId) allTransfers)

        traitConsistencyViolations : List String
        traitConsistencyViolations =
            List.filterMap Tuple.second transferlists

        transferListToStays : List Transfer -> ( List Stay, List String )
        transferListToStays tl =
            case tl of
                [] ->
                    ( [], [] )

                [ _ ] ->
                    ( [], [] )

                a :: b :: rest ->
                    let
                        ( stays, violations ) =
                            transferListToStays (b :: rest)
                    in
                    case ( a.kind, b.kind ) of
                        ( In _, In _ ) ->
                            ( stays, violations ++ [ "Consistency constraint violated for animal: " ++ a.animal.animalId ++ " Two Intakes without Outcome inbetween." ] )

                        ( Out _, Out _ ) ->
                            ( stays, violations ++ [ "Consistency constraint violated for animal: " ++ a.animal.animalId ++ " Two Outcomes without Intake inbetween." ] )

                        ( Out _, In _ ) ->
                            ( stays, violations )

                        ( In intake, Out outcome ) ->
                            let
                                stay =
                                    { animal = a.animal
                                    , durationDays = DateTime.getDayDiff (DateTime.fromPosix a.date) (DateTime.fromPosix b.date)
                                    , inDay = a.date
                                    , inAge = intake.age
                                    , outcomeType = outcome.outcomeType
                                    }
                            in
                            ( stays ++ [ stay ], violations )

        --(List Stay, List String)
        ( transfers, inOutConsistencyViolations ) =
            List.foldl (\( a, b ) ( c, d ) -> ( a ++ c, b ++ d ))
                ( [], [] )
                (List.map transferListToStays (List.filterMap Tuple.first transferlists))
    in
    ( transfers, traitConsistencyViolations ++ inOutConsistencyViolations )


stayErrors : List Intake -> List Outcome -> ( List Stay, List String )
stayErrors i o =
    transfersToStays (transferListFromInOut i o)
