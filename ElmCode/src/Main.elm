module Main exposing (..)

import Axis
import Browser
import Bulma.CDN exposing (stylesheet)
import Bulma.Columns as BCo
import Bulma.Components as BC
import Bulma.Elements as BE
import Bulma.Form as BF
import Bulma.Layout as BL
import Bulma.Modifiers as BM
import Bulma.Modifiers.Typography as BMT
import Calendar exposing (Date)
import Color
import Csv.Decode
import Dict
import Helper exposing (drawTuplePosition, normalizeFloat, scaleHeight, scaleWidth)
import Html exposing (Html, a, text)
import Html.Attributes exposing (pattern)
import Html.Events
import Html.Lazy
import Http
import List.Extra
import List.Statistics
import Platform.Cmd as Cmd
import RecursivePattern exposing (Level(..), PixelPositon(..), RecordedData(..), augementLevel, createPixelMap, startPosition)
import Scale
import Scale.Color
import ShelterData exposing (Animal, Coat, Intake, Outcome, Stay)
import Time
import TypedSvg exposing (g, svg, text_)
import TypedSvg.Attributes exposing (class, fontFamily, fontSize, textAnchor, transform, viewBox, x, y)
import TypedSvg.Attributes.InPx
import TypedSvg.Core exposing (Svg)
import TypedSvg.Events
import TypedSvg.Filters.Attributes exposing (radius)
import TypedSvg.Types exposing (AnchorAlignment(..), Length(..), Transform(..))


main : Program () Model Msg
main =
    Browser.document { init = init, update = update, view = view, subscriptions = subscriptions }


gitlab : Bool
gitlab =
    True


gitLabPagesAffix : String
gitLabPagesAffix =
    if gitlab then
        "/visualisierung-austinshelterdata"

    else
        ""


loadData : List ( Result Http.Error String -> Msg, String )
loadData =
    [ ( GotInText, "/Data/Austin Animal Center Intakes since2020.csv" )
    , ( GotOutText, "/Data/Austin Animal Center Outcomes since2020.csv" )
    ]


type alias Model =
    { errors : List String
    , data :
        { coatData : Maybe Coat
        , inData : Maybe (List Intake)
        , outData : Maybe (List Outcome)
        , stays : Maybe (List Stay)
        }
    , selectedAnimals : List (List String)
    , siteProps :
        { warning : Bool
        , modalOpen : Bool
        , dropdownActive : Bool
        }
    , boxPlotProps :
        { boxPlotFunction : BoxPlotFunction
        , onlyAdopts : Bool
        }
    }


type BoxPlotFunction
    = ColorPatterns
    | MainColorPatterns
    | Purebreed
    | AgeOnIntake
    | Breed
    | Sex
    | AnimalType


allBPF : List BoxPlotFunction
allBPF =
    [ ColorPatterns, MainColorPatterns, Purebreed, AgeOnIntake, Breed, Sex, AnimalType ]


stringFromBoxPlotFunction : BoxPlotFunction -> String
stringFromBoxPlotFunction bpf =
    case bpf of
        ColorPatterns ->
            "Included Color and Pattern"

        MainColorPatterns ->
            "Main Color and Pattern"

        Purebreed ->
            "Purebreed"

        AgeOnIntake ->
            "Age upon Intake"

        Breed ->
            "Included Breed"

        Sex ->
            "Sex"

        AnimalType ->
            "Animal Type"


init : () -> ( Model, Cmd Msg )
init _ =
    ( { errors = []
      , data =
            { coatData = Nothing
            , inData = Nothing
            , outData = Nothing
            , stays = Nothing
            }
      , selectedAnimals = []
      , siteProps =
            { modalOpen = False
            , warning = True
            , dropdownActive = False
            }
      , boxPlotProps =
            { boxPlotFunction = ColorPatterns
            , onlyAdopts = True
            }
      }
    , Http.get
        { url = gitLabPagesAffix ++ "/Data/Coat.json"
        , expect = Http.expectJson GotCoatText ShelterData.coatDecoder
        }
    )


type Msg
    = GotCoatText (Result Http.Error Coat)
    | GotInText (Result Http.Error String)
    | GotOutText (Result Http.Error String)
    | ChangeSelected (List String)
    | DeleteMsg
    | ModalChange Bool
    | ToggleDropdown
    | ChangeBPF BoxPlotFunction
    | AdoptChange


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        --Coat.json text loaded and decoded (or failed)
        GotCoatText result ->
            updateGotCoat model result

        --Intakes csv text loaded (or failed)
        GotInText result ->
            updateGotIn model result

        --Outcomes csv text loaded (or failed)
        GotOutText result ->
            updateGotOut model result

        --Selected Animals changed
        ChangeSelected list ->
            updateSelectedAnimals model list

        DeleteMsg ->
            let
                oSiteprops =
                    model.siteProps
            in
            ( { model
                | siteProps = { oSiteprops | warning = False }
              }
            , Cmd.none
            )

        ModalChange bool ->
            let
                oSiteprops =
                    model.siteProps
            in
            ( { model
                | siteProps = { oSiteprops | modalOpen = bool }
              }
            , Cmd.none
            )

        ToggleDropdown ->
            let
                oSiteprops =
                    model.siteProps
            in
            ( { model
                | siteProps = { oSiteprops | dropdownActive = not oSiteprops.dropdownActive }
              }
            , Cmd.none
            )

        ChangeBPF bpf ->
            let
                oBoxPlotProps =
                    model.boxPlotProps

                oSiteprops =
                    model.siteProps
            in
            ( { model
                | siteProps = { oSiteprops | dropdownActive = False }
                , boxPlotProps = { oBoxPlotProps | boxPlotFunction = bpf }
              }
            , Cmd.none
            )

        AdoptChange ->
            let
                oBoxPlotProps =
                    model.boxPlotProps
            in
            ( { model
                | boxPlotProps = { oBoxPlotProps | onlyAdopts = not oBoxPlotProps.onlyAdopts }
              }
            , Cmd.none
            )


updateGotCoat : Model -> Result Http.Error Coat -> ( Model, Cmd Msg )
updateGotCoat model result =
    let
        odata =
            model.data
    in
    case result of
        Ok coatdata ->
            let
                newcoat =
                    { coatdata | patterns = "solid" :: "bicolor" :: "multicolor" :: coatdata.patterns }
            in
            ( { model | data = { odata | coatData = Just newcoat } }
            , Cmd.batch <|
                List.map
                    (\( m, a ) ->
                        Http.get
                            { url = gitLabPagesAffix ++ a
                            , expect = Http.expectString m
                            }
                    )
                    loadData
            )

        Err _ ->
            ( { model | errors = model.errors ++ [ "Failed to load Coat.json" ] }
            , Cmd.none
            )


updateGotIn : Model -> Result Http.Error String -> ( Model, Cmd Msg )
updateGotIn model result =
    let
        odata =
            model.data
    in
    case result of
        Ok raw ->
            case model.data.coatData of
                Just coat ->
                    let
                        ( mDataIn, errors ) =
                            ShelterData.decodeIntake raw coat.patterns coat.colors
                    in
                    case ( mDataIn, model.data.outData ) of
                        ( Just indata, Just outdata ) ->
                            let
                                ( stays, stayerrors ) =
                                    ShelterData.stayErrors indata outdata

                                data =
                                    { odata | inData = Just indata, stays = Just stays }
                            in
                            ( { model | data = data, errors = model.errors ++ errors ++ stayerrors }
                            , Cmd.none
                            )

                        _ ->
                            let
                                data =
                                    { odata | inData = mDataIn }
                            in
                            ( { model | data = data, errors = model.errors ++ errors }
                            , Cmd.none
                            )

                Nothing ->
                    ( { model | errors = model.errors ++ [ "Failed to decode 'Austin Animal Center Intakes.csv' without Coat.json" ] }
                    , Cmd.none
                    )

        Err _ ->
            ( { model | errors = model.errors ++ [ "Failed to load 'Austin Animal Center Intakes.csv'" ] }
            , Cmd.none
            )


updateGotOut : Model -> Result Http.Error String -> ( Model, Cmd Msg )
updateGotOut model result =
    let
        odata =
            model.data
    in
    case result of
        Ok raw ->
            case model.data.coatData of
                Just coat ->
                    let
                        ( mDataOut, errors ) =
                            ShelterData.decodeOutcome raw coat.patterns coat.colors
                    in
                    case ( mDataOut, model.data.inData ) of
                        ( Just outdata, Just indata ) ->
                            let
                                ( stays, stayerrors ) =
                                    ShelterData.stayErrors indata outdata

                                data =
                                    { odata | stays = Just stays, outData = Just outdata }
                            in
                            ( { model | data = data, errors = model.errors ++ errors ++ stayerrors }
                            , Cmd.none
                            )

                        _ ->
                            let
                                data =
                                    { odata | outData = mDataOut }
                            in
                            ( { model | data = data, errors = model.errors ++ errors }
                            , Cmd.none
                            )

                Nothing ->
                    ( { model | errors = model.errors ++ [ "Failed to decode 'Austin Animal Center Coutcomes.csv' without Coat.json" ] }
                    , Cmd.none
                    )

        Err _ ->
            ( { model | errors = model.errors ++ [ "Failed to load 'Austin Animal Center Outcomes.csv'" ] }
            , Cmd.none
            )


updateSelectedAnimals : Model -> List String -> ( Model, Cmd Msg )
updateSelectedAnimals model list =
    if list == [] then
        ( { model | selectedAnimals = [] }, Cmd.none )

    else
        let
            newList =
                List.filter (\l -> not (List.Extra.isPrefixOf list l || List.Extra.isPrefixOf l list)) model.selectedAnimals
        in
        if List.member list model.selectedAnimals then
            ( { model | selectedAnimals = newList }, Cmd.none )

        else
            ( { model | selectedAnimals = list :: newList }, Cmd.none )


checkAnimalSelected : Animal -> List (List String) -> Bool
checkAnimalSelected animal listlist =
    let
        animallist : List String
        animallist =
            List.map (\g -> g animal) getter
    in
    List.any (\l -> List.Extra.isPrefixOf l animallist) listlist


navlevel : Html Msg
navlevel =
    BC.navbar
        { color = BM.Primary, transparent = False }
        []
        [ BC.navbarStart []
            [ BC.navbarItem False
                []
                [ BE.title BE.H2
                    [ BMT.textColor BMT.White ]
                    [ text "Austin Animal Center Visualization" ]
                ]
            ]
        , BC.navbarMenu True
            []
            [ BC.navbarStart []
                []
            , BC.navbarEnd [] []
            ]
        ]


warn : List String -> Html Msg
warn list =
    if list /= [] then
        BC.message { color = BM.Danger, size = BM.Standard }
            []
            [ BC.messageHeaderWithDelete []
                DeleteMsg
                [ Html.p [] [ text "The following errors and inconsistencies ocurred:" ]
                ]
            , BC.messageBody []
                [ Html.ul []
                    (List.map (\e -> Html.li [] [ text e ])
                        list
                    )
                ]
            ]

    else
        Html.div [] []


maybeDataToString : Maybe (List a) -> String
maybeDataToString mlist =
    case mlist of
        Just list ->
            String.fromInt (List.length list) ++ " entries"

        Nothing ->
            "Loading"


levelInfos : Model -> Html Msg
levelInfos model =
    BL.level []
        [ BL.levelItem [ BMT.textCentered ]
            [ Html.div []
                [ Html.p [ class [ "heading" ] ] [ text "Coat colors" ]
                , BE.title BE.H5 [] [ text (maybeDataToString (Maybe.map .colors model.data.coatData)) ]
                ]
            ]
        , BL.levelItem [ BMT.textCentered ]
            [ Html.div []
                [ Html.p [ class [ "heading" ] ] [ text "Coat patterns" ]
                , BE.title BE.H5 [] [ text (maybeDataToString (Maybe.map .patterns model.data.coatData)) ]
                ]
            ]
        , BL.levelItem [ BMT.textCentered ]
            [ Html.div []
                [ Html.p [ class [ "heading" ] ] [ text "Intakes" ]
                , BE.title BE.H5 [] [ text (maybeDataToString model.data.inData) ]
                ]
            ]
        , BL.levelItem [ BMT.textCentered ]
            [ Html.div []
                [ Html.p [ class [ "heading" ] ] [ text "Outcomes" ]
                , BE.title BE.H5 [] [ text (maybeDataToString model.data.outData) ]
                ]
            ]
        , BL.levelItem [ BMT.textCentered ]
            [ Html.div []
                [ Html.p [ class [ "heading" ] ] [ text "Stays" ]
                , BE.title BE.H5 [] [ text (maybeDataToString model.data.stays) ]
                ]
            ]
        ]


onlyAdoptButton : Bool -> Html Msg
onlyAdoptButton onlyAdopt =
    let
        mods =
            BE.buttonModifiers
    in
    case onlyAdopt of
        True ->
            BE.connectedButtons BM.Left
                []
                [ BE.button { mods | state = BM.Active, color = BM.Primary } [] [ Html.text "only Adoptions" ]
                , BE.button mods [ Html.Events.onClick AdoptChange ] [ text "all Outcomes" ]
                ]

        False ->
            BE.connectedButtons BM.Centered
                []
                [ BE.button mods [ Html.Events.onClick AdoptChange ] [ Html.text "only Adoptions" ]
                , BE.button { mods | state = BM.Active, color = BM.Primary } [] [ text "all Outcomes" ]
                ]


view : Model -> Browser.Document Msg
view model =
    let
        dicts =
            case ( model.data.inData, model.data.outData ) of
                ( Just i, Just o ) ->
                    let
                        filterin =
                            if List.length model.selectedAnimals > 0 then
                                List.filter (\e -> checkAnimalSelected e.animal model.selectedAnimals) i

                            else
                                i

                        filterout =
                            if List.length model.selectedAnimals > 0 then
                                List.filter (\e -> checkAnimalSelected e.animal model.selectedAnimals) o

                            else
                                o
                    in
                    Just (dictsFromInsOuts filterin filterout)

                _ ->
                    Nothing

        pixelview =
            case dicts of
                Just ( i, o ) ->
                    let
                        allvalues =
                            List.map Tuple.second (Dict.toList i)
                                ++ List.map Tuple.second (Dict.toList o)

                        scale =
                            Helper.normalizeFloat (List.filterMap (Maybe.map toFloat) allvalues)
                    in
                    [ BE.title BE.H4
                        []
                        [ text "Intakes per day" ]
                    , Html.Lazy.lazy2 pixel (Dict.toList i) scale
                    , BE.title BE.H4
                        []
                        [ text "Outcomes per day" ]
                    , Html.Lazy.lazy2 pixel (Dict.toList o) scale
                    ]

                Nothing ->
                    case ( model.data.inData, model.data.outData ) of
                        ( Just _, Just _ ) ->
                            [ Html.text "Something went wrong" ]

                        _ ->
                            [ Html.text "Something has not been loaded yet" ]

        treemapView =
            case model.data.inData of
                Just inData ->
                    let
                        animals =
                            List.map .animal inData
                    in
                    [ BE.box []
                        [ BE.title BE.H4
                            []
                            [ text "Intakes Composition" ]
                        , Html.p [] [ text "Clicking on rectangles selects the fitting animals for the other visualizations." ]
                        , Html.Lazy.lazy2 mosaic model.selectedAnimals animals
                        ]
                    ]

                Nothing ->
                    []

        boxplotview : List (Html Msg)
        boxplotview =
            case ( model.data.stays, model.data.coatData ) of
                ( Just stays, Just c ) ->
                    [ BE.box []
                        [ BE.title BE.H4
                            []
                            [ text "Days in Shelter, only Stays completely in Data" ]
                        , BF.fields BM.Left
                            []
                            [ BF.control BF.controlModifiers [] [ boxPlotFunctionDropdown model.siteProps.dropdownActive model.boxPlotProps.boxPlotFunction ]
                            , BF.control BF.controlModifiers [] [ onlyAdoptButton model.boxPlotProps.onlyAdopts ]
                            , BF.control BF.controlModifiers
                                []
                                [ Html.Lazy.lazy4 BE.easyButton
                                    BE.buttonModifiers
                                    []
                                    (ModalChange True)
                                    "Explanation"
                                ]
                            ]
                        , Html.p [] [ text "Try choosing a different subset of animals, if there are too many categories to read in the plot." ]
                        , Html.Lazy.lazy3 drawBoxPlot (filterstays model.boxPlotProps.onlyAdopts stays model.selectedAnimals) model.boxPlotProps.boxPlotFunction c
                        ]
                    ]

                _ ->
                    []

        warning : Bool -> List (Html Msg)
        warning b =
            if b then
                [ Html.Lazy.lazy warn model.errors ]

            else
                []
    in
    { title = "Austin Shelter Data Visualization"
    , body =
        stylesheet
            :: Html.Lazy.lazy (\_ -> navlevel) ()
            :: Html.Lazy.lazy levelInfos model
            :: warning model.siteProps.warning
            ++ [ Html.Lazy.lazy3 BC.modal
                    model.siteProps.modalOpen
                    []
                    [ BC.easyModalBackground [] (ModalChange False)
                    , BC.modalContent []
                        [ BE.box []
                            [ Html.ul []
                                [ Html.li [] [ text "Upper Whisker: 99th Percentile" ]
                                , Html.li [] [ text "Lower Whisker: 1st Percentile" ]
                                , Html.li [] [ text "Upper Box Boundary: Upper Quartile" ]
                                , Html.li [] [ text "Lower Box Boundary: Lower Quartile" ]
                                , Html.li [] [ text "Horizontal Line: Median" ]
                                , Html.li [] [ text "Blue Circle: Mean" ]
                                , Html.li [] [ text "White Circles: Outlier" ]
                                ]
                            ]
                        ]
                    , BC.easyModalClose BM.Large [] (ModalChange False)
                    ]
               , BL.tileAncestor
                    BM.Auto
                    []
                    [ BL.tileParent BM.Auto
                        []
                        [ BL.tileParent BM.Auto
                            []
                            [ BL.tileChild BM.Auto
                                []
                                treemapView
                            ]
                        ]
                    ]
               , BL.tileAncestor
                    BM.Auto
                    []
                    [ BL.tileParent BM.Auto
                        []
                        [ BL.tileParent BM.Auto
                            []
                            [ BL.tileChild BM.Auto
                                []
                                [ BE.box []
                                    pixelview
                                ]
                            ]
                        ]
                    ]
               , BL.tileAncestor
                    BM.Auto
                    []
                    [ BL.tileParent BM.Auto
                        []
                        [ BL.tileParent BM.Auto
                            []
                            [ BL.tileChild BM.Auto
                                []
                                boxplotview
                            ]
                        ]
                    ]
               ]
    }


boxPlotFunctionDropdown : Bool -> BoxPlotFunction -> Html Msg
boxPlotFunctionDropdown b bpf =
    BC.dropdown
        b
        BC.dropdownModifiers
        []
        [ BC.dropdownTrigger
            []
            [ BE.button BE.buttonModifiers
                [ Html.Events.onClick ToggleDropdown
                , Html.Attributes.attribute "aria-haspopup" "true"
                , Html.Attributes.attribute "aria-controls" "dropdown-menu"
                ]
                [ text "Select Animal Trait"
                ]
            ]
        , BC.dropdownMenu []
            []
            (List.map (\e -> BC.dropdownItem (e == bpf) [ Html.Events.onClick (ChangeBPF e) ] [ text (stringFromBoxPlotFunction e) ]) allBPF)
        ]


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


type alias CurrentRecordedData msg =
    RecordedData ( String, Maybe Float ) (List (TypedSvg.Core.Attribute msg))



--############################################################################################
--Pixel


pixel : List ( String, Maybe Int ) -> Scale.ContinuousScale Float -> Html Msg
pixel liste scale =
    let
        c : Int
        c =
            ceiling (toFloat (List.length liste) / (7 * 4 * 13))

        level : List Level
        level =
            [ RecursivePattern.Level 1 c
            , RecursivePattern.Level 13 1
            , RecursivePattern.Level 1 4
            , RecursivePattern.Level 7 1
            , RecursivePattern.Level 1 1
            ]

        whfromLevel : List Level -> ( Int, Int )
        whfromLevel levellist =
            case levellist of
                [] ->
                    ( 0, 0 )

                [ RecursivePattern.Level a b ] ->
                    ( a, b )

                (RecursivePattern.Level a b) :: restlist ->
                    let
                        ( ra, rb ) =
                            whfromLevel restlist
                    in
                    ( a * ra, b * rb )

        ( wtemp, htemp ) =
            whfromLevel level

        ( w, h ) =
            ( 100, ceiling (100 / toFloat wtemp * toFloat htemp) )

        padding =
            0.75

        paddingup =
            1

        pixelList : List PixelPositon
        pixelList =
            createPixelMap startPosition (augementLevel level)

        currentData : List ( String, Maybe Float )
        currentData =
            List.map
                (\( a, b ) ->
                    ( a ++ ": " ++ Maybe.withDefault "no data" (Maybe.map String.fromInt b)
                    , Maybe.map toFloat b
                    )
                )
                (List.sortBy Tuple.first liste)

        ourData : List (CurrentRecordedData msg)
        ourData =
            List.map2 (\a b -> RecordedData a b []) pixelList currentData

        createStyle : ( String, Maybe Float ) -> List (TypedSvg.Core.Attribute msg)
        createStyle ( str, mval ) =
            case mval of
                Just val ->
                    [ TypedSvg.Attributes.title str
                    , TypedSvg.Attributes.fill <|
                        TypedSvg.Types.Paint <|
                            (Scale.Color.infernoInterpolator
                                << Scale.convert scale
                            )
                                val
                    ]

                Nothing ->
                    [ TypedSvg.Attributes.title str
                    , TypedSvg.Attributes.fill <|
                        TypedSvg.Types.Paint <|
                            Color.gray
                    ]

        drawStyle : CurrentRecordedData msg -> CurrentRecordedData msg
        drawStyle (RecordedData pixelPosition value attributeList) =
            RecordedData pixelPosition value (List.append attributeList (createStyle value))

        drawPosition : CurrentRecordedData msg -> CurrentRecordedData msg
        drawPosition (RecordedData pixelPosition value _) =
            RecordedData pixelPosition value (drawTuplePosition ( w, h ) level pixelPosition)

        draw : CurrentRecordedData msg -> Svg msg
        draw (RecordedData pixelPosition value attributeList) =
            g [ class [ "pixel" ], fontSize <| Px 1.5, fontFamily [ "sans-serif" ] ]
                [ TypedSvg.rect
                    attributeList
                    []
                , text_
                    [ textAnchor AnchorMiddle, x <| Px (toFloat w / 2 + padding), y <| Px (-paddingup / 2) ]
                    [ text (Tuple.first value) ]
                ]
    in
    TypedSvg.svg
        [ TypedSvg.Attributes.viewBox 0 0 (toFloat w + 2 * padding) (toFloat h + 2 * padding + paddingup)
        , TypedSvg.Attributes.width <| TypedSvg.Types.Percent 100
        , TypedSvg.Attributes.height <| TypedSvg.Types.Percent 100
        , TypedSvg.Attributes.preserveAspectRatio (TypedSvg.Types.Align TypedSvg.Types.ScaleMin TypedSvg.Types.ScaleMin) TypedSvg.Types.Slice
        ]
        [ TypedSvg.style [] [ TypedSvg.Core.text """
            .pixel text { display: none; }
            .pixel:hover rect { stroke-width: 0.1; stroke: rgb(118, 214, 78);  }
            .pixel:hover text { display: inline; }
          """ ]
        , TypedSvg.g [ transform [ Translate padding (padding + paddingup) ] ] <| List.map (drawPosition >> drawStyle >> draw) ourData
        ]


getter : List (ShelterData.Animal -> String)
getter =
    [ .animalType
    , sexToString << .sex
    , purebreedToString << .purebreed
    ]


monthToInt : Time.Month -> Int
monthToInt month =
    case month of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12


dictsFromInsOuts : List Intake -> List Outcome -> ( Dict.Dict String (Maybe Int), Dict.Dict String (Maybe Int) )
dictsFromInsOuts ins outs =
    let
        inMillis =
            List.map (\i -> Time.posixToMillis i.inDate) ins

        outMillis =
            List.map (\i -> Time.posixToMillis i.outDate) outs

        minmaxIn : Maybe ( Int, Int )
        minmaxIn =
            List.Statistics.minmax inMillis

        minmaxOut : Maybe ( Int, Int )
        minmaxOut =
            List.Statistics.minmax outMillis

        dateRange : List Calendar.Date
        dateRange =
            getDateRange minmaxIn
                minmaxOut

        dateToString : Calendar.Date -> String
        dateToString date =
            (String.padLeft 2 '0' <| String.fromInt (Calendar.getYear date))
                ++ "/"
                ++ (String.padLeft 2 '0' <| String.fromInt (monthToInt (Calendar.getMonth date)))
                ++ "/"
                ++ (String.padLeft 2 '0' <| String.fromInt (Calendar.getDay date))

        compCount : ( String, List String ) -> ( String, Maybe Int )
        compCount ( d, list ) =
            ( d, Just (List.length list + 1) )

        inDict =
            Dict.fromList <|
                List.map
                    compCount
                    (List.Extra.gatherEquals <|
                        List.map (\m -> dateToString <| Calendar.fromPosix <| Time.millisToPosix m) inMillis
                    )

        outDict =
            Dict.fromList <|
                List.map
                    compCount
                    (List.Extra.gatherEquals <|
                        List.map (\m -> dateToString <| Calendar.fromPosix <| Time.millisToPosix m) outMillis
                    )

        allDateDictIn =
            case minmaxIn of
                Just mmI ->
                    Dict.fromList <|
                        List.map
                            (\d ->
                                if Tuple.first mmI <= Calendar.toMillis d && Calendar.toMillis d <= Tuple.second mmI then
                                    ( dateToString d, Just 0 )

                                else
                                    ( dateToString d, Nothing )
                            )
                            dateRange

                Nothing ->
                    Dict.fromList <|
                        List.map
                            (\d ->
                                ( dateToString d, Just 0 )
                            )
                            dateRange

        allDateDictOut =
            case minmaxOut of
                Just mmO ->
                    Dict.fromList <|
                        List.map
                            (\d ->
                                if Tuple.first mmO <= Calendar.toMillis d && Calendar.toMillis d <= Tuple.second mmO then
                                    ( dateToString d, Just 0 )

                                else
                                    ( dateToString d, Nothing )
                            )
                            dateRange

                Nothing ->
                    Dict.fromList <|
                        List.map
                            (\d ->
                                ( dateToString d, Just 0 )
                            )
                            dateRange
    in
    ( Dict.union inDict allDateDictIn, Dict.union outDict allDateDictOut )


getDateRange : Maybe ( Int, Int ) -> Maybe ( Int, Int ) -> List Calendar.Date
getDateRange minmaxIn minmaxOut =
    let
        minmaxAll =
            case ( minmaxIn, minmaxOut ) of
                ( Just mmI, Just mmO ) ->
                    Just
                        ( min (Tuple.first mmI) (Tuple.first mmO)
                        , max (Tuple.second mmI) (Tuple.second mmO)
                        )

                ( Nothing, Just mmO ) ->
                    Just mmO

                ( Just mmI, Nothing ) ->
                    Just mmI

                ( Nothing, Nothing ) ->
                    Nothing

        minmaxDates : Maybe ( Calendar.Date, Calendar.Date )
        minmaxDates =
            Maybe.map (\( a, b ) -> ( Calendar.fromPosix <| Time.millisToPosix a, Calendar.fromPosix <| Time.millisToPosix b )) minmaxAll

        firstDay =
            Maybe.andThen
                (\d ->
                    Calendar.fromRawParts
                        { day = 1, month = Time.Jan, year = Calendar.getYear d }
                )
                (Maybe.map Tuple.first minmaxDates)

        lastDay =
            Maybe.andThen
                (\d ->
                    Calendar.fromRawParts
                        { day = 31, month = Time.Dec, year = Calendar.getYear d }
                )
                (Maybe.map Tuple.second minmaxDates)
    in
    Maybe.withDefault [] <| Maybe.map2 Calendar.getDateRange firstDay lastDay



--########################################################################################
--Mosaik


type Axis
    = XAxis
    | YAxis


sexToString : Maybe ShelterData.Sex -> String
sexToString ms =
    case ms of
        Nothing ->
            "unknown"

        Just ShelterData.Female ->
            "Female"

        Just ShelterData.Male ->
            "Male"


purebreedToString : Bool -> String
purebreedToString p =
    if p then
        "Purebreed"

    else
        "not Purebreed"


mosaic : List (List String) -> List Animal -> Html Msg
mosaic selectedLists animals =
    let
        start =
            { x = 0, y = 0 }

        ( w, h ) =
            ( 1000, 1000 )

        padding =
            50

        paddingtext =
            50

        ( svgs, xcapts, ycapts ) =
            submosaic
                { colorScale = Scale.linear ( 1, 0.5 ) ( 0, toFloat <| List.length getter )
                , captionPosition = ( start.x + w / 2, start.y - padding / 5 )
                }
                "All Animals"
                []
                selectedLists
                animals
                getter
                { x1 = start.x, y1 = start.y, x2 = start.x + w, y2 = start.y + h }
                XAxis
    in
    svg
        [ viewBox (start.x - padding) (start.y - padding) (w + 2 * padding + paddingtext) (h + 2 * padding + paddingtext)
        , TypedSvg.Attributes.width <| TypedSvg.Types.Percent 50
        , TypedSvg.Attributes.height <| TypedSvg.Types.Percent 50
        ]
        [ TypedSvg.style [] [ TypedSvg.Core.text """
            .area text { display: none; }
            .area:hover text { display: inline; }

            .area.activated rect { stroke-width: 0.1; fill: maroon;  }
            .area:hover rect { stroke-width: 0.1; fill: firebrick;  }
            .area.activated:hover rect { stroke-width: 0.1; fill: darkred;  }
            
          """ ]
        , g
            []
            svgs
        , g [ transform [ Translate 0 (h + padding / 2) ] ]
            (List.indexedMap
                (\index1 list ->
                    let
                        b =
                            toFloat <| Maybe.withDefault 0 <| List.maximum (List.map List.length xcapts)
                    in
                    g
                        []
                        (List.indexedMap
                            (\index2 str ->
                                text_
                                    [ textAnchor AnchorMiddle
                                    , x <| Px ((toFloat index2 + 0.5) * (w - toFloat index1 * 100) / b)
                                    , y <| Px (30 * toFloat index1)
                                    , fontFamily [ "Helvetica", "sans-serif" ]
                                    , fontSize <| Px 20
                                    ]
                                    [ text str ]
                            )
                            list
                        )
                )
                (List.filter (\x -> not (List.any (\e -> List.length x < List.length e && List.Extra.isSubsequenceOf x e) xcapts)) xcapts)
            )
        , g
            [ transform [ Translate (w + padding) 0 ] ]
            (List.indexedMap
                (\index1 list ->
                    let
                        b =
                            toFloat <| Maybe.withDefault 0 <| List.maximum (List.map List.length ycapts)
                    in
                    g
                        []
                        (List.indexedMap
                            (\index2 str ->
                                text_
                                    [ textAnchor AnchorMiddle
                                    , y <| Px ((toFloat index2 + 0.5) * (h - toFloat index1 * 100) / b)
                                    , x <| Px (22 * toFloat index1)
                                    , fontFamily [ "Helvetica", "sans-serif" ]
                                    , fontSize <| Px 20
                                    ]
                                    [ text str ]
                            )
                            list
                        )
                )
                (List.filter (\x -> not (List.any (\e -> List.length x < List.length e && List.Extra.isSubsequenceOf x e) ycapts)) ycapts)
            )
        ]


type alias SubmosaicInfo msg =
    { index : Int
    , sum : Int
    , xCapsList : List (List String)
    , yCapsList : List (List String)
    , svgs : List (Svg msg)
    }


type alias SubmosaicConfig =
    { colorScale : Scale.ContinuousScale Float
    , captionPosition : ( Float, Float )
    }


type alias Area =
    { x1 : Float
    , y1 : Float
    , x2 : Float
    , y2 : Float
    }


submosaic : SubmosaicConfig -> String -> List String -> List (List String) -> List Animal -> List (Animal -> String) -> Area -> Axis -> ( List (Svg Msg), List (List String), List (List String) )
submosaic conf name currentList selectedLists animals getters area axis =
    let
        paint =
            TypedSvg.Types.Paint <|
                Color.fromHsla
                    { hue = 0
                    , saturation = 0
                    , alpha = 1
                    , lightness = Scale.convert conf.colorScale (toFloat <| List.length getters)
                    }

        namesvg =
            text_
                [ textAnchor AnchorMiddle
                , x <| Px <| Tuple.first conf.captionPosition
                , y <| Px <| Tuple.second conf.captionPosition
                , fontFamily [ "Helvetica", "sans-serif" ]
                , fontSize <| Px 20
                , TypedSvg.Attributes.fill <| TypedSvg.Types.Paint Color.darkRed
                ]
                [ text (name ++ ": " ++ (String.fromInt <| List.length animals) ++ " animals") ]

        classes =
            if List.any (\l -> List.Extra.isPrefixOf l currentList) selectedLists then
                [ "area", "activated" ]

            else
                [ "area" ]
    in
    case getters of
        [] ->
            let
                drawRect : Svg Msg
                drawRect =
                    TypedSvg.rect
                        [ TypedSvg.Attributes.InPx.y area.y1
                        , TypedSvg.Attributes.InPx.x area.x1
                        , TypedSvg.Attributes.InPx.width (area.x2 - area.x1)
                        , TypedSvg.Attributes.InPx.height (area.y2 - area.y1)
                        , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                        , TypedSvg.Attributes.fill paint
                        , TypedSvg.Attributes.title name
                        , TypedSvg.Events.onClick (ChangeSelected currentList)
                        ]
                        []
            in
            ( [ TypedSvg.g
                    [ class classes ]
                    [ drawRect, namesvg ]
              ]
            , []
            , []
            )

        g :: r ->
            let
                max =
                    toFloat (List.length animals)

                ip =
                    10

                valsCount =
                    List.sortBy Tuple.first (List.map (\( a, b ) -> ( a, List.length b + 1 )) <| List.Extra.gatherEquals (List.map g animals))

                vals =
                    List.map Tuple.first valsCount

                padding =
                    toFloat (List.length vals + 1) * ip

                --inner padding
                scale : Scale.ContinuousScale Float
                scale =
                    case axis of
                        XAxis ->
                            Scale.linear ( area.x1, area.x2 - padding ) ( 0, max )

                        YAxis ->
                            Scale.linear ( area.y1, area.y2 - padding ) ( 0, max )

                newAxis : Axis
                newAxis =
                    case axis of
                        XAxis ->
                            YAxis

                        YAxis ->
                            XAxis

                newArea : Int -> Int -> Int -> { x1 : Float, y1 : Float, x2 : Float, y2 : Float }
                newArea a b index =
                    let
                        pad =
                            toFloat <| index * ip
                    in
                    case axis of
                        XAxis ->
                            { x1 = (Scale.convert scale <| toFloat a) + pad
                            , x2 = Scale.convert scale (toFloat b) + pad
                            , y1 = area.y1 + ip
                            , y2 = area.y2 - ip
                            }

                        YAxis ->
                            { y1 = (Scale.convert scale <| toFloat a) + pad
                            , y2 = Scale.convert scale (toFloat b) + pad
                            , x1 = area.x1 + ip
                            , x2 = area.x2 - ip
                            }

                --------------------------------------------------------------------------
                restSvgs : SubmosaicInfo Msg
                restSvgs =
                    List.foldl
                        drawForRest
                        { index = 1
                        , sum = 0
                        , xCapsList = []
                        , yCapsList = []
                        , svgs = []
                        }
                        vals

                drawForRest : String -> SubmosaicInfo Msg -> SubmosaicInfo Msg
                drawForRest str si =
                    let
                        parts =
                            List.filter (\a -> g a == str) animals

                        valSum =
                            List.length parts + si.sum

                        ( svgs, xli, yli ) =
                            submosaic conf str (currentList ++ [ str ]) selectedLists parts r (newArea si.sum valSum si.index) newAxis
                    in
                    { index = si.index + 1
                    , sum = valSum
                    , xCapsList = List.Extra.unique (xli ++ si.xCapsList)
                    , yCapsList = List.Extra.unique (yli ++ si.yCapsList)
                    , svgs = si.svgs ++ svgs
                    }

                drawRect : Svg Msg
                drawRect =
                    TypedSvg.rect
                        [ TypedSvg.Attributes.InPx.y area.y1
                        , TypedSvg.Attributes.InPx.x area.x1
                        , TypedSvg.Attributes.InPx.width (area.x2 - area.x1)
                        , TypedSvg.Attributes.InPx.height (area.y2 - area.y1)
                        , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                        , TypedSvg.Attributes.fill paint
                        , TypedSvg.Attributes.title name
                        , TypedSvg.Events.onClick (ChangeSelected currentList)
                        ]
                        []

                ( xlist, ylist ) =
                    case axis of
                        XAxis ->
                            ( vals, [] )

                        YAxis ->
                            ( [], vals )
            in
            ( TypedSvg.g
                [ class classes ]
                [ drawRect, namesvg ]
                :: restSvgs.svgs
            , xlist :: restSvgs.xCapsList
            , ylist :: restSvgs.yCapsList
            )



--################################################################
--boxplots


filterstays : Bool -> List Stay -> List (List String) -> List Stay
filterstays onlyAdopts stays selectedAnimals =
    if onlyAdopts then
        if List.length selectedAnimals > 0 then
            List.filter (\s -> checkAnimalSelected s.animal selectedAnimals && s.outcomeType == "Adoption") stays

        else
            List.filter (\s -> s.outcomeType == "Adoption") stays

    else if List.length selectedAnimals > 0 then
        List.filter (\s -> checkAnimalSelected s.animal selectedAnimals) stays

    else
        stays


drawBoxPlot : List Stay -> BoxPlotFunction -> Coat -> Html Msg
drawBoxPlot stays bpf c =
    case bpf of
        ColorPatterns ->
            let
                colorlist =
                    List.map
                        (\col ->
                            ( col
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        List.any
                                            (\e -> e == col)
                                            s.animal.coat.colors
                                    )
                                    stays
                                )
                            )
                        )
                        c.colors

                patternlist =
                    List.map
                        (\col ->
                            ( col
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        List.any
                                            (\e -> e == col)
                                            s.animal.coat.patterns
                                    )
                                    stays
                                )
                            )
                        )
                        c.patterns
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Colors" ]
                , boxplots (( "all", List.map .durationDays stays ) :: colorlist)
                , Html.p [] [ text "days in Shelter by Patterns" ]
                , boxplots (( "all", List.map .durationDays stays ) :: patternlist)
                ]

        MainColorPatterns ->
            let
                colorlist =
                    List.map
                        (\col ->
                            ( col
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        List.head s.animal.coat.colors == Just col
                                    )
                                    stays
                                )
                            )
                        )
                        c.colors

                patternlist =
                    List.map
                        (\pat ->
                            ( pat
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        List.head s.animal.coat.patterns == Just pat
                                    )
                                    stays
                                )
                            )
                        )
                        c.patterns
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Main Colors" ]
                , boxplots (( "all", List.map .durationDays stays ) :: colorlist)
                , Html.p [] [ text "days in Shelter by Main Patterns" ]
                , boxplots (( "all", List.map .durationDays stays ) :: patternlist)
                ]

        Purebreed ->
            let
                stringlist =
                    List.Extra.unique (List.map (\s -> purebreedToString s.animal.purebreed) stays)

                datalist =
                    List.map
                        (\e ->
                            ( e
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        purebreedToString s.animal.purebreed == e
                                    )
                                    stays
                                )
                            )
                        )
                        stringlist
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Purebreed" ]
                , boxplots (( "all", List.map .durationDays stays ) :: datalist)
                ]

        Breed ->
            let
                stringlist =
                    List.Extra.unique (List.concat (List.map (\s -> s.animal.breeds) stays))

                datalist =
                    List.map
                        (\e ->
                            ( e
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        List.member e s.animal.breeds
                                    )
                                    stays
                                )
                            )
                        )
                        stringlist
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Breed" ]
                , boxplots (( "all", List.map .durationDays stays ) :: datalist)
                ]

        AgeOnIntake ->
            let
                intlist =
                    [ 84, 180, 365, 720 ]

                recordlist : List { age : Int, durationDays : Int }
                recordlist =
                    List.filterMap
                        (\x ->
                            case x.inAge of
                                Just a ->
                                    Just { age = a, durationDays = x.durationDays }

                                Nothing ->
                                    Nothing
                        )
                        stays

                datalist =
                    filterByAge intlist recordlist
                        ++ [ ( "Unknown", List.map .durationDays (List.filter (\x -> x.inAge == Nothing) stays) ) ]
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Age Upon Intake" ]
                , boxplots (( "all", List.map .durationDays stays ) :: datalist)
                ]

        Sex ->
            let
                stringlist : List String
                stringlist =
                    List.Extra.unique (List.map (\s -> sexToString s.animal.sex) stays)

                datalist =
                    List.map
                        (\e ->
                            ( e
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        sexToString s.animal.sex == e
                                    )
                                    stays
                                )
                            )
                        )
                        stringlist
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Sex" ]
                , boxplots (( "all", List.map .durationDays stays ) :: datalist)
                ]

        AnimalType ->
            let
                stringlist : List String
                stringlist =
                    List.Extra.unique (List.map (\s -> s.animal.animalType) stays)

                datalist =
                    List.map
                        (\e ->
                            ( e
                            , List.map
                                (\element -> element.durationDays)
                                (List.filter
                                    (\s ->
                                        s.animal.animalType == e
                                    )
                                    stays
                                )
                            )
                        )
                        stringlist
            in
            Html.div []
                [ Html.p [] [ text "days in Shelter by Animal Type" ]
                , boxplots (( "all", List.map .durationDays stays ) :: datalist)
                ]


filterByAge : List Int -> List { age : Int, durationDays : Int } -> List ( String, List Int )
filterByAge agelist staylist =
    case agelist of
        [] ->
            [ ( "Rest", List.map .durationDays staylist ) ]

        i :: rest ->
            let
                ( yes, no ) =
                    List.partition (\x -> x.age <= i) staylist
            in
            ( "<=" ++ String.fromInt i ++ " days", List.map .durationDays yes ) :: filterByAge rest no


boxplots : List ( String, List Int ) -> Html msg
boxplots datalist =
    let
        filteredDataList =
            List.filter (\e -> Tuple.second e /= []) datalist

        tickCount =
            5

        padding =
            100

        ( w, h ) =
            ( 1000, 500 )

        ( min, max ) =
            Maybe.withDefault ( 0, 0 ) (List.Statistics.minmax (List.concat (List.map Tuple.second filteredDataList)))

        xScale : Scale.ContinuousScale Float
        xScale =
            Scale.linear ( 0, w ) ( -0.5, toFloat (List.length filteredDataList) - 0.5 )

        yScale : Scale.ContinuousScale Float
        yScale =
            Scale.linear ( h, 0 ) ( toFloat min, toFloat max )

        boxes =
            List.indexedMap (\i e -> drawBox (Tuple.second e) i xScale yScale) filteredDataList
    in
    svg
        [ viewBox 0 0 (w + 2 * padding) (h + 2 * padding)
        , TypedSvg.Attributes.width <| TypedSvg.Types.Percent 75
        , TypedSvg.Attributes.height <| TypedSvg.Types.Percent 75
        ]
        [ g [ transform [ Translate padding (padding + h) ] ] [ Axis.bottom [ Axis.tickCount 0 ] xScale ]
        , g [ transform [ Translate padding padding ] ] [ Axis.left [ Axis.tickCount tickCount ] yScale ]
        , g [ transform [ Translate padding padding ] ]
            (List.concat boxes)
        , g [ transform [ Translate padding (padding + h) ] ]
            (List.indexedMap
                (\i e ->
                    text_
                        [ textAnchor AnchorMiddle
                        , x <| Px (Scale.convert xScale (toFloat i))
                        , y <| Px ((1 + (toFloat <| modBy 3 i)) * (padding / 4))
                        ]
                        [ text
                            (Tuple.first e
                                ++ ": "
                                ++ String.fromInt (List.length (Tuple.second e))
                            )
                        ]
                )
                filteredDataList
            )
        ]


drawBox : List Int -> Int -> Scale.ContinuousScale Float -> Scale.ContinuousScale Float -> List (Svg msg)
drawBox valuelist index xScale yScale =
    case valuelist of
        [] ->
            []

        a :: _ ->
            let
                whiskerwidth =
                    0.3

                boxwidth =
                    0.6

                mid =
                    Scale.convert xScale (toFloat index)

                whiskerLeft =
                    Scale.convert xScale (toFloat index - whiskerwidth * 0.5)

                whiskerRight =
                    Scale.convert xScale (toFloat index + whiskerwidth * 0.5)

                boxLeft =
                    Scale.convert xScale (toFloat index - boxwidth * 0.5)

                boxRight =
                    Scale.convert xScale (toFloat index + boxwidth * 0.5)

                valuelistFloat =
                    List.sort <| List.map toFloat valuelist

                mean =
                    Maybe.withDefault (toFloat a) <| List.Statistics.mean valuelistFloat

                median =
                    Maybe.withDefault (toFloat a) <| List.Statistics.median valuelistFloat

                p005 =
                    Maybe.withDefault (toFloat a) <| List.Statistics.percentile 0.01 valuelistFloat

                p095 =
                    Maybe.withDefault (toFloat a) <| List.Statistics.percentile 0.99 valuelistFloat

                p025 =
                    Maybe.withDefault (toFloat a) <| List.Statistics.percentile 0.25 valuelistFloat

                p075 =
                    Maybe.withDefault (toFloat a) <| List.Statistics.percentile 0.75 valuelistFloat

                outlier =
                    List.filter (\e -> e > p095 || e < p005) valuelistFloat
            in
            [ TypedSvg.line
                -- WhiskerVertical
                [ TypedSvg.Attributes.x1 <| Px mid
                , TypedSvg.Attributes.y1 <| Px (Scale.convert yScale p005)
                , TypedSvg.Attributes.x2 <| Px mid
                , TypedSvg.Attributes.y2 <| Px (Scale.convert yScale p095)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                , TypedSvg.Attributes.strokeWidth <| Px 1
                ]
                []
            , TypedSvg.line
                -- upperWhisker
                [ TypedSvg.Attributes.x1 <| Px whiskerLeft
                , TypedSvg.Attributes.y1 <| Px (Scale.convert yScale p095)
                , TypedSvg.Attributes.x2 <| Px whiskerRight
                , TypedSvg.Attributes.y2 <| Px (Scale.convert yScale p095)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                , TypedSvg.Attributes.strokeWidth <| Px 1
                ]
                []
            , TypedSvg.line
                -- lowerWhisker
                [ TypedSvg.Attributes.x1 <| Px whiskerLeft
                , TypedSvg.Attributes.y1 <| Px (Scale.convert yScale p005)
                , TypedSvg.Attributes.x2 <| Px whiskerRight
                , TypedSvg.Attributes.y2 <| Px (Scale.convert yScale p005)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                , TypedSvg.Attributes.strokeWidth <| Px 1
                ]
                []
            , TypedSvg.polygon
                --box
                [ TypedSvg.Attributes.points
                    [ ( boxLeft, Scale.convert yScale p025 )
                    , ( boxLeft, Scale.convert yScale p075 )
                    , ( boxRight, Scale.convert yScale p075 )
                    , ( boxRight, Scale.convert yScale p025 )
                    ]
                , TypedSvg.Attributes.fill (TypedSvg.Types.Paint Color.darkRed)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                ]
                []
            , TypedSvg.line
                -- Median
                [ TypedSvg.Attributes.x1 <| Px boxLeft
                , TypedSvg.Attributes.y1 <| Px (Scale.convert yScale median)
                , TypedSvg.Attributes.x2 <| Px boxRight
                , TypedSvg.Attributes.y2 <| Px (Scale.convert yScale median)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                , TypedSvg.Attributes.strokeWidth <| Px 2
                ]
                []
            , TypedSvg.circle
                [ TypedSvg.Attributes.cx <| Px mid
                , TypedSvg.Attributes.cy <| Px (Scale.convert yScale mean)
                , TypedSvg.Attributes.r <| Px 4
                , TypedSvg.Attributes.fill (TypedSvg.Types.Paint Color.lightBlue)
                , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                , TypedSvg.Attributes.strokeWidth <| Px 1
                ]
                []
            ]
                ++ List.map
                    (\o ->
                        TypedSvg.circle
                            [ TypedSvg.Attributes.cx <| Px mid
                            , TypedSvg.Attributes.cy <| Px (Scale.convert yScale o)
                            , TypedSvg.Attributes.r <| Px 4
                            , TypedSvg.Attributes.fill TypedSvg.Types.PaintNone
                            , TypedSvg.Attributes.stroke (TypedSvg.Types.Paint Color.black)
                            , TypedSvg.Attributes.strokeWidth <| Px 1
                            ]
                            []
                    )
                    outlier
