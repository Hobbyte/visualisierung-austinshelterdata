# Visualisierung-AustinShelterData


## Data
Daten über Aufnahmen und Abgaben von Tieren im Tierheim:

https://www.kaggle.com/datasets/carrie535/animal-shelter-intake-and-outcome?select=Austin+Animal+Center+Intakes.csv


## Fragestellung

1. Sind schwarze Tiere (insbesondere Katzen) unbeliebter als Tiere anderer Farben?
    * Bleiben sie länger im Tierheim?
2. Ist die Kittenzeit deutlich erkennbar im zeitlichen Verlauf der Aufnahmen/Anzahl Tiere im Tierheim? Gibt es weitere Muster?
3. Wie setzen sich die aufgenommenen Tiere zusammen? Welcher Anteil der Tierheimhunde sind Rassetiere?

## Stand

aktueller Stand unter:
    https://hobbyte.gitlab.io/visualisierung-austinshelterdata/

- [x] Daten einlesen, aus Performancegründen momentan nur 2020
- [x] Pixelorientiert: Anzahl Aufnahmen/Abgaben über Zeit
- [x] Mosaic Plot über Tierheimbelegung
- [x] BoxPlot über Tage im Tierheim
- [x] Bericht


